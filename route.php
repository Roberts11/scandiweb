<?php

use Controller\ItemController;
use Controller\AddController;

$page = $_GET['page'] ?? null;

switch ($page){
    case 'product-list':
        (new ItemController)->index();
    break;
    case 'create-product-item':
        (new ItemController)->store();
    break;
    case 'delete-product-item':
        (new ItemController)->delete();
    break;
    case 'add':
        (new AddController)->index();
    break;
    default:
        header('location:?page=product-list');
}
