<?php 
    $this->yield('layout/head.php'); 
    $this->yield('layout/user-navigation.php'); 
?>

<div class="container mb-3 mt-3">
    <div class="row d-flex justify-content-between">
        <div class="ml-3">
            <h3>Product list </h3>
        </div>
        <div class="mr-3">
            <form class="d-inline" method="POST" action="?page=product-list">
                <select id="mySelect" name="options">
                    <option value="All">Show all</option>
                    <option value="dvd">DVD-disc</option>
                    <option value="book">Book</option>
                    <option value="furniture">Furniture</option>
                </select>
                <div class="d-inline">
                    <button class="btn btn-primary ml-3" type="submit" name="select">Apply</button>
                </div>
            </form>
        </div>
    </div>
    <hr>
</div>


<div class="container mt-3">
    <div class="row">
    
        <?php foreach ($items as $item): ?>
            <div class="col-lg-3 DVD-disc">
                <div class="card text-white bg-secondary mb-3" style="max-width: 18rem;">
                    <div class="card-body">
                        <div class="d-flex justify-content-end">
                            <a class="btn btn-secondary btn-delete-product" href="?page=delete-product-item&id=<?= $item['id'] ?>">X</a>
                        </div>
                        <div class="card-title"><?= $item['sku']; ?></div>
                        <div class="card-text"><?= $item['name']; ?></div>
                        <div class="card-text"><?= $item['price']; ?> $</div>

                        <?php if($item['type'] == 'dvd'): ?>
                        <p class="card-text">Size: <?= $item['size']; ?> Mb</p>
                        <?php elseif($item['type'] == 'book'): ?>       
                        <p class="card-text">Weight: <?= $item['weight']; ?> Kg</p>
                        <?php elseif($item['type'] == 'furniture'): ?>       
                        <p class="card-text">Dimention: <?= $item['height']; ?> X <?= $item['width']; ?> X <?= $item['length']; ?></p>
                        <?php else: ?>       
                        <p class="card-text">Oops! Something went wrong!</p>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>

    </div>
</div>

<?php $this->yield('layout/footer.php'); ?>