<?php 
    $this->yield('layout/head.php'); 
    $this->yield('layout/user-navigation.php'); 
?>

<form name="addProduct" action="?page=create-product-item" method="post" onsubmit="return validateForm()">
<div class="container mb-3 mt-3">
    <div class="row d-flex justify-content-between">
      <h3 class="ml-3">Product Add</h3>
      <button type="submit" class="mr-3 btn btn-primary" name="Submit" value="Add">Save</button>
    </div>
  <hr>
</div>

<div class="container">
      <div class="row">
        <div class="col-7">
            <div class="form-group row">
              <div class="col-sm-3">
                <label class="col-form-label" for="sku">SKU</label>
              </div>
              <div class="col-sm-9">
                <input type="text" name="sku" class="form-control" id="sku">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-3">
                <label class="col-form-label" for="name">Name</label>
              </div>
              <div class="col-sm-9">
                <input type="text" name="name" class="form-control" id="name">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-3">
                <label class="col-form-label" for="price">Price</label>
              </div>
              <div class="col-sm-9">
                <input type="text" name="price" class="form-control" id="price">
              </div>
            </div>

            <div class="row d-flex justify-content-between">
              <p class="ml-3">Type Switcher</p>
              <div class="dropdown mr-3">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Type Switcher
                </button>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                  <a id="showDvd" class="dropdown-item" href="#">DVD-disc</a>
                  <a id="showBook" class="dropdown-item" href="#">Book</a>
                  <a id="showFurniture" class="dropdown-item" href="#">Furniture</a>
                </div>
              </div>
            </div>


            <div class="form-group row">
                <input type="hidden" class="form-control" name="type" value="dvd" id="type">
            </div>

            <div id="dvd-add" class="form-group row mt-3 transition">
              <div class="col-sm-3">
                <label class="col-form-label" for="size">Size</label>
              </div>
              <div class="col-sm-9">
                <input type="text" class="form-control" name="size" id="size" placeholder="Please provide DVD-disc size, Mb">
              </div>
              <p class="col mt-3">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Illo et neque dolorum at, veniam sapiente quis architecto obcaecati iure optio!</p>
            </div>

            <div id="furniture-add" class="form-group row mt-3 d-none transition">
              <div class="col-sm-3">
                <label class="col-form-label" for="height">Height</label>
              </div>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-3" name="height" id="height" placeholder="Please provide furniture height">
              </div>
              <label class="col-sm-3 col-form-label" for="width">Width</label>
              <div class="col-sm-9">
                <input type="text" class="form-control mb-3" name="width" id="width" placeholder="Please provide furniture width">
              </div>
              <label class="col-sm-3 col-form-label" for="length">length</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" name="length" id="length" placeholder="Please provide furniture length">
              </div>
              <p class="col mt-3">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Illo et neque dolorum at, veniam sapiente quis architecto obcaecati iure optio!</p>
            </div>

            <div id="book-add" class="form-group row mt-3 d-none transition">
              <label class="col-sm-3 col-form-label" for="weight">Weight</label>
              <div class="col-sm-9">
                <input type="text" name="weight" class="form-control" id="weight" placeholder="Please provide book weight, Kg">
              </div>
              <p class="col mt-3">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Illo et neque dolorum at, veniam sapiente quis architecto obcaecati iure optio!</p>
            </div>

          </form>
        </div>

      </div>
    </div>


<?php $this->yield('layout/footer.php'); ?>