<?php

require 'config.php';

spl_autoload_register(function ($className) {
    $filePath = str_replace('\\',DS,APPLICATION_ROOT.$className.'.php');
    require_once $filePath;
});

require 'route.php';