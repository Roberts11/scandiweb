<?php
namespace Controller;

use Framework\Template;
use Repository\ItemMysqlRepository;

class ItemController
{

    protected $template;
    protected $itemMysqlRepository;

    public function __construct()
    {
        $this->template = new Template;
        $this->itemMysqlRepository = new ItemMysqlRepository;
    }

    public function index()
    {
        if(!isset($_POST['select'])){
            $this->template->assign([
                'items' => $this->itemMysqlRepository->fetchAll(),
            ])->render('product-list.php');
        } else {
            $type = $_POST['options'];
    
            if($type == "All") {
                $this->template->assign([
                    'items' => $this->itemMysqlRepository->fetchAll(),
                ])->render('product-list.php');
                   
            } else {
                $this->template->assign([
                    'items' => $this->itemMysqlRepository->fetchByType($type),
                ])->render('product-list.php');
            }
            
        }
    }

    public function store()
    {
        $this->itemMysqlRepository->store();
        header('location:?page=product-list');
    }

    public function delete()
    {
        $this->itemMysqlRepository->deleteById($_GET['id']);
        header('location:?page=product-list');
    }
}