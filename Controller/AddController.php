<?php
namespace Controller;

use Framework\Template;

class AddController
{
    protected $template;

    public function __construct()
    {
        $this->template = new Template;
    }

    public function index()
    {
        $this->template->render('add.php');
    }
}