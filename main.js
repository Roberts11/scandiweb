// Form validation

function emptyFurnitureInput() {
  let isFurnitureFieldsEmpty = false;
  let heightInput = document.forms["addProduct"]["height"].value;
  let widthInput = document.forms["addProduct"]["width"].value;
  let lengthInput = document.forms["addProduct"]["length"].value;

  if (heightInput == "") {
    isFurnitureFieldsEmpty = true;
  } else if (widthInput == "") {
    isFurnitureFieldsEmpty = true;
  } else if (lengthInput == "") {
    isFurnitureFieldsEmpty = true;
  }
  return isFurnitureFieldsEmpty;
}

function validateForm() {
  let skuInput = document.forms["addProduct"]["sku"].value;
  let nameInput = document.forms["addProduct"]["name"].value;
  let priceInput = document.forms["addProduct"]["price"].value;
  let dvdSizeInput = document.forms["addProduct"]["size"].value;
  let bookWeightInput = document.forms["addProduct"]["weight"].value;

  if (skuInput == "") {
    alert("Please provide SKU");
    return false;
  } else if (nameInput == "") {
    alert("Please provide name");
    return false;
  } else if (priceInput == "") {
    alert("Please provide price");
    return false;
  } else if (
    dvdSizeInput == "" &&
    bookWeightInput == "" &&
    emptyFurnitureInput() == true
  ) {
    alert("Please provide proper atribute values");
    return false;
  }
}

// Combine show/hide functions

$(document).ready(function() {
  $("#showDvd").click(function() {
    hideAll();
    showDvd();
    setOptionDvd();
  });
});

$(document).ready(function() {
  $("#showBook").click(function() {
    hideAll();
    showBook();
    setOptionBook();
  });
});

$(document).ready(function() {
  $("#showFurniture").click(function() {
    hideAll();
    showFurniture();
    setOptionFurniture();
  });
});

// Product Add page show/hide functionality

function hideAll() {
  $("#dvd-add, #book-add, #furniture-add").removeClass("d-flex");
  $("#dvd-add, #book-add, #furniture-add")
    .addClass("d-none")
    .animate(
      {
        opacity: 0
      },
      200
    );
}

function showDvd() {
  $("#dvd-add")
    .addClass("d-flex")
    .animate(
      {
        opacity: 1
      },
      400
    );
}

function showBook() {
  $("#book-add")
    .addClass("d-flex")
    .animate(
      {
        opacity: 1
      },
      400
    );
}

function showFurniture() {
  $("#furniture-add")
    .addClass("d-flex")
    .animate(
      {
        opacity: 1
      },
      400
    );
}

// Set hidden type input field value

function setOptionDvd() {
  $("#type").val("dvd");
}

function setOptionBook() {
  $("#type").val("book");
}

function setOptionFurniture() {
  $("#type").val("furniture");
}
