<?php
namespace Framework;

class Template
{
    protected $data = [];

    public function assign($data){
        $this->data = $data;
        return $this;
    }

    public function yield($filePath)
    {
        $this->render($filePath);
    }

    public function render($filePath)
    {
        extract($this->data);
        require TEMPLATE_ROOT.$filePath;
    }
}