<?php
namespace Repository;

use PDO;

class ItemMysqlRepository
{
    protected $database;

    public function __construct()
    {
        $this->database = new PDO(DB_CDN,DB_USERNAME,DB_PASSWORD);
    }

    public function fetchAll()
    {
        $selectAllQuery = $this->database->prepare('SELECT * FROM products ORDER BY id DESC');
        $selectAllQuery->execute();

        return $selectAllQuery->fetchAll();
    }

    public function fetchByType($type)
    {
        $selectQuery = $this->database->prepare('SELECT * FROM products WHERE type=:type ORDER BY id DESC');
        $selectQuery->bindValue(':type',$type);
        $selectQuery->execute();

        return $selectQuery->fetchAll();
    }

    public function store()
    {
        $queryInsert = $this->database->prepare('INSERT INTO products (sku, name, price, type, size, weight, height, width, length)VALUES(:sku,:name,:price,:type,:size,:weight,:height,:width,:length)');
        $queryInsert->bindValue(':sku', $_POST['sku']);
        $queryInsert->bindValue(':name', $_POST['name']);
        $queryInsert->bindValue(':price', $_POST['price']);
        $queryInsert->bindValue(':type', $_POST['type']);
        $queryInsert->bindValue(':size', $_POST['size']);
        $queryInsert->bindValue(':weight', $_POST['weight']);
        $queryInsert->bindValue(':height', $_POST['height']);
        $queryInsert->bindValue(':width', $_POST['width']);
        $queryInsert->bindValue(':length', $_POST['length']);
        $queryInsert->execute();
    }

    public function deleteById($id)
    {
        $queryDelete = $this->database->prepare('DELETE FROM products WHERE id=:id');
        $queryDelete->bindValue(':id',$id);
        $queryDelete->execute();
    }
}